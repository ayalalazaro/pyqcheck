# pyqcheck

Python code for communicating with PTW QUICKCHECK and downloading measurements as pandas DataFrame.

## Requirements: 
python 3.x
numpy, pandas and tqdm (pip install)

## Usage:
### Download all the measurements in QUICKCHECK device:
###
    
    from qcheck import QUICKCHECK
    qc = QUICKCHECK('QUICKCHECK-XXX')      QUICKCHECK-XXX (your QUICKCHECK device hostname or IP)
	qc.connect()
	qc.get_measurements()
	
    Receiving Quickcheck measurements
    100%|██████████| 108/108 [00:01<00:00, 58.57it/s]
###
	qc.measurements.tail()
	 
     AV_BQF_Max  AV_BQF_Min    ...           WORK_ID  WORK_Name
    103       103.0        97.0    ...      1.714130e+09    SYNERGY
    104       103.0        97.0    ...      1.714130e+09    SYNERGY
    105       103.0        97.0    ...      1.714130e+09    SYNERGY
    106       103.0        97.0    ...      1.396219e+09   INFINITY
    107       102.5        97.5    ...      1.396219e+09   INFINITY


# -*- coding: utf-8 -*-
"""
Class based QUICKCHECK connection

@author: Rafael Ayala
"""
import socket, codecs, re, datetime
import numpy as np
import pandas as pd
from tqdm import tqdm

class QUICKCHECK():
    
    def __init__(self, ip):
        self.ip = ip
        self.port = 8123
        
    def connect(self):
        print("UDP target IP:", self.ip)
        print("UDP target port:", self.port)
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP
        self.sock.settimeout(6)  
        self.send_quickcheck('SER')
        print(self.data)
    
    def close(self):
        self.sock.close()
        del self.sock
    
    def prepare_qcheck(self):
        self.MSG = self.MSG.encode() + codecs.decode("0d", "hex") + codecs.decode("0a", "hex")
    
    def send_quickcheck(self, MESSAGE):
        self.MSG = MESSAGE
        #print("message:", self.MSG)
        self.prepare_qcheck()
        max_retries = 3
        n_retry = 0
        while True:
            try:
                self.sock.sendto(self.MSG, (self.ip, self.port))
                self.raw_data, self.addr = self.sock.recvfrom(4096)
                #print('Server says:')
                #print(raw_data)
                data = self.raw_data.decode(encoding='utf-8')
                self.data = data.strip('\r\n')
                break
            except:
                if n_retry == max_retries:
                    print('Timeout')
                    self.data =[]
                    break
                else:
                    n_retry += n_retry
                    continue
                
    def parse_measurements(self):
        data_split = self.data.split(';')
        if data_split[0] == 'MEASGET':
            m ={} #Dictionary with measurements
            ##MD section:__________________________________________________________
            MD = re.findall('MD=\[(.*?)\]', self.data)[0]
            m['MD_ID'] = np.int(re.findall('ID=(.*?);', MD)[0])
            meas_date = re.findall('Date=(.*?);', MD)[0]
            m['MD_Date'] = datetime.datetime.strptime(meas_date, "%Y-%m-%d").date()
            meas_time = re.findall('Time=(.*?)$', MD)[0]
            m['MD_Time'] = datetime.datetime.strptime(meas_time, "%H:%M:%S").time()
            m['MD_DateTime']=datetime.datetime.combine(m['MD_Date'], 
                          m['MD_Time'])
            ##MV section:__________________________________________________________
            str_val = re.findall('MV=\[(.*?)\]', self.data)[0]
            m['MV_CAX'] = np.float(re.findall('CAX=(.*?);', str_val)[0])
            m['MV_G10'] = np.float(re.findall('G10=(.*?);', str_val)[0])
            m['MV_L10'] = np.float(re.findall('L10=(.*?);', str_val)[0])
            m['MV_T10'] = np.float(re.findall('T10=(.*?);', str_val)[0])
            m['MV_R10'] = np.float(re.findall('R10=(.*?);', str_val)[0])
            m['MV_G20'] = np.float(re.findall('G20=(.*?);', str_val)[0])
            m['MV_L20'] = np.float(re.findall('L20=(.*?);', str_val)[0])
            m['MV_T20'] = np.float(re.findall('T20=(.*?);', str_val)[0])
            m['MV_R20'] = np.float(re.findall('R20=(.*?);', str_val)[0])
            m['MV_E1'] = np.float(re.findall('E1=(.*?);', str_val)[0])
            m['MV_E2'] = np.float(re.findall('E2=(.*?);', str_val)[0])
            m['MV_E3'] = np.float(re.findall('E3=(.*?);', str_val)[0])
            m['MV_E4'] = np.float(re.findall('E4=(.*?);', str_val)[0])
            m['MV_Temp'] = np.float(re.findall('Temp=(.*?);', str_val)[0])
            m['MV_Press'] = np.float(re.findall('Press=(.*?);', str_val)[0])
            m['MV_CAXRate'] = np.float(re.findall('CAXRate=(.*?);', str_val)[0])
            m['MV_ExpTime'] = np.float(re.findall('ExpTime=(.*?)$', str_val)[0])
            
            ##AV section:__________________________________________________________
            AV = re.findall('AV=\[(.*?)\]\]', self.data)[0]
            AV = AV + ']' # add last character ]
            for s in ('CAX', 'FLAT', 'SYMGT', 'SYMLR', 'BQF','We'):
                str_val = re.findall(s + '=\[(.*?)\]', AV)[0]
                m['AV_'+s+'_Min'] = np.float(re.findall('Min=(.*?);', str_val)[0])
                m['AV_'+s+'_Max'] = np.float(re.findall('Max=(.*?);', str_val)[0])
                m['AV_'+s+'_Target'] = np.float(re.findall('Target=(.*?);', str_val)[0])
                m['AV_'+s+'_Norm'] = np.float(re.findall('Norm=(.*?);', str_val)[0])
                m['AV_'+s+'_Value'] = np.float(re.findall('Value=(.*?);', str_val)[0])
                m['AV_'+s+'_Valid'] = np.int(re.findall('Valid=(.*?)$', str_val)[0])
            
            ##WORK section:__________________________________________________________
            str_val = re.findall('WORK=\[(.*?)\]', self.data)[0]
            
            m['WORK_ID'] = np.int(re.findall('ID=(.*?);', str_val)[0])
            m['WORK_Name'] = re.findall('Name=(.*?)$', str_val)[0]
            
            ##TASK section:__________________________________________________________
            str_val = re.findall('TASK=\[(.*?)\];MV', self.data)[0]
            m['TASK_ID'] = np.int(re.findall('ID=(.*?);', str_val)[0])
            m['TASK_TUnit'] = re.findall('TUnit=(.*?);', str_val)[0]
            m['TASK_En'] = np.int(re.findall('En=(.*?);', str_val)[0])
            m['TASK_Mod'] = re.findall('Mod=(.*?);', str_val)[0]
            m['TASK_Fs'] = re.findall('Fs=(.*?);', str_val)[0]
            m['TASK_SSD'] = np.int(re.findall('SDD=(.*?);', str_val)[0])
            m['TASK_Ga'] = np.int(re.findall('Ga=(.*?);', str_val)[0])
            m['TASK_We'] = np.int(re.findall('We=(.*?);', str_val)[0])
            m['TASK_MU'] = np.int(re.findall('MU=(.*?);', str_val)[0])
            m['TASK_My'] = np.float(re.findall('My=(.*?);', str_val)[0])
            m['TASK_Info'] = re.findall('Info=(.*?)$', str_val)[0]
            
            str_val = re.findall('Prot=\[(.*?)\];', str_val)[0]
            m['TASK_Prot_Name'] = re.findall('Name=(.*?);', str_val)[0]
            m['TASK_Prot_Flat'] = np.int(re.findall('Flat=(.*?);', str_val)[0])
            m['TASK_Prot_Sym'] = np.int(re.findall('Sym=(.*?)$', str_val)[0])
            return m
        elif data_split[0] == 'MEASCNT':
            m={}
            m[data_split[0]]= np.int(data_split[1:][0])
            return m
        elif data_split[0] in ('PTW', 'SER', 'KEY'):
            m={}
            m[data_split[0]]= data_split[1:]
            return m
        
    def get_measurements(self):
        self.send_quickcheck('MEASCNT')
        m = self.parse_measurements()
        try:
            n_meas = m['MEASCNT']
        except:
            self.send_quickcheck('MEASCNT')
            m = self.parse_measurements()
            n_meas = m['MEASCNT']
        print('Receiving Quickcheck measurements')
        l = []
        for m in tqdm(range(n_meas)):
           self.send_quickcheck('MEASGET;INDEX-MEAS='+ '%02d' % (m,))
           meas = self.parse_measurements()
           l.append(meas)
           #print(meas)
        self.measurements = pd.DataFrame(l)